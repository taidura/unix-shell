#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>


#define TSH_BUFFER_SIZE 20
#define TSH_ARG_SIZE 20


void tsh_loop(void);
char * read_line(void);
char ** parse_line(char * line);
int exec_argv(char **);


int main(void) {

    tsh_loop();

    return EXIT_SUCCESS;

}

void tsh_loop(void) {

    int status;
    char * line;
    char ** argv;

    while(1) {
        fprintf(stdout, "%s@%s:%s > ", getenv("LOGNAME"), getenv("NAME"), getenv("PWD"));
        line = read_line();
        argv = parse_line(line);
        status = exec_argv(argv);
    }       
}


char * read_line(void) {

    char * buffer;
    int position = 0;
    int bufsize = TSH_BUFFER_SIZE;
    int c;

    buffer = malloc(sizeof(char) * TSH_BUFFER_SIZE);
    if (buffer == NULL) {
        fprintf(stderr, "Error in malloc()\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        c = getchar();
        if ((c == EOF) || (c == '\n')) {
            buffer[position] = '\0';
            return buffer;
        } else {
            buffer[position] = c;
        }
        position ++;

        if (position >= bufsize) {
            bufsize += TSH_BUFFER_SIZE;
            buffer = realloc(buffer, bufsize);
            if (buffer == NULL) {
                fprintf(stderr, "Error in realloc()\n");
                exit(EXIT_FAILURE);
            }
        }
    }
}

char ** parse_line(char * line) {

    int c;
    int i;
    int len = strlen(line);
    char * ptr;
    int position = 0;
    int arg_position = 0;
    char ** argv;
    int argv_size = TSH_ARG_SIZE;

    argv = malloc(sizeof(char *) * argv_size);
    if (argv == NULL) {
        fprintf(stdout, "Error in malloc()\n");
        exit(EXIT_FAILURE);
    }

    argv[argv_size - 1] = NULL;
    ptr = line;
    while (position <= len + 1) {
        line[len + 1] = ' ';
        if (line[position] == ' ') {
            line[position] = '\0';
            argv[arg_position] = malloc(strlen(ptr) + 1);
            strcpy(argv[arg_position], ptr);
            arg_position ++;
            line[position] = ' ';
            ptr = &line[position + 1];
        }
        position ++;
        line[len + 1] = '\0';
        if (arg_position == argv_size) {
            argv_size += TSH_ARG_SIZE;
            argv = realloc(argv, sizeof(char *) * argv_size);
            argv[argv_size - 1] = NULL;
            if (argv == NULL) {
                fprintf(stderr, "Error in realloc()\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    return argv;
}


int exec_argv(char ** argv) {

    pid_t pid_child;
    char * bin_path = "/bin/";
    char * full_path = malloc(strlen(bin_path) + strlen(argv[0]) + 1);
    int status;
    if (full_path == NULL) {
        fprintf(stdout, "Error in malloc()\n");
        exit(EXIT_FAILURE);
    }
    strcpy(full_path, bin_path);
    strcat(full_path, argv[0]);

    if ((pid_child = fork()) < 0) {
        perror("fork:");
        exit(EXIT_FAILURE);
    }

    if (pid_child == 0) {
        execv(full_path, argv);
        fprintf(stderr, "Something went wrong with execv()\n");
        exit(EXIT_FAILURE);
    } else {
        while(waitpid(pid_child, &status, 0) > 0) {
            if (WIFEXITED(status)) {
                return(WEXITSTATUS(status));
            } else {
                fprintf(stdout, "Something went wrong in the child process \n");
                exit(EXIT_FAILURE);
            }
        }
    }
}
